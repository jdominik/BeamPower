/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.controller;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;

/**
 *
 * @author RENT
 */
@Named(value = "sessionController")
@SessionScoped
public class SessionController implements Serializable {
    private Integer counter = 0;

    public void setCounter(Integer counter) {
        this.counter = counter++;
    }

    public Integer getCounter() {
        return counter;
    }
    

    /**
     * Creates a new instance of SessionController
     */
    public SessionController() {
        counter++;
        
    }
    
    
    
}
