package com.mycompany.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.NewJSFManagedBean;
import com.mycompany.model.Post;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "formguest")
@RequestScoped
public class ApplicationControllerGuest {

    private Post post = new Post();     // to jest obiekt DTO
    private String user;
    private String title;
    private String text;

    public void setPost(Post post) {
        this.post = post;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public String getUser() {
        return user;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    @ManagedProperty(value = "#{applicationController}")
    private NewJSFManagedBean applicationController;    // wstrzykiwanie innego kontrolera, by wziąć z niego listę i przetworzyć w naszym kontrolerze

    /**
     * Creates a new instance of applicationControllerGuest
     */
    public NewJSFManagedBean getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(NewJSFManagedBean applicationController) {
        this.applicationController = applicationController;
    }


    public void save() {
        EntityManager em = applicationController.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Post post = new Post();
        post.setUser(this.getUser());
        post.setTitle(this.getText());
        post.setText(this.getText());
        em.persist(post);           // wołam persist zamiast save, idea działania identyczna jak w hibernate, tylko zmieniają się nazwy metod
        transaction.commit();

        System.out.println("Saving to database");

    }

}
