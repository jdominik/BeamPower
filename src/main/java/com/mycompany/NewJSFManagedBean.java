package com.mycompany;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.model.Post;
import java.util.List;
import javax.faces.bean.ApplicationScoped;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.*;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class NewJSFManagedBean {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    private String version = "60beta";

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    /**
     * Creates a new instance of NewJSFManagedBean
     */
    public NewJSFManagedBean() {
        emf = Persistence.createEntityManagerFactory("pu");  //powiązanie z bazą danych poprzez powiedzenie fabryce, by zrobiła entty managera
        em = emf.createEntityManager();

    }

    public EntityManager getEntityManager() {
        return em;
    }
    
    public List<Post> getList() {
        Query query = em.createQuery("FROM Post");
        return query.getResultList();
    }


}
